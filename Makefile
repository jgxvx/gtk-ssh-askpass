.PHONY: release
release:
	cargo build --release

.PHONY: install
install:
	cp -fv ./target/release/gtk-ssh-askpass /usr/bin/gtk-ssh-askpass

.PHONY: clean
clean:
	cargo clean


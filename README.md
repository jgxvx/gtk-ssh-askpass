# gtk-ssh-askpass

Simple SSH askpass dialog built with gtk-rs.

## Building

### Using Cargo

#### For Development
```shell
cargo build
```

#### For Release
```shell
cargo build --release
```

### Using Makefile

The default target will create a release build:
```shell
make
```

## Installation

First, create a release build and then copy, move or symlink the binary from `./target/release/gtk-ssh-askpass` to a location in your `PATH`.

The `install` target will copy the binary to `/usr/bin/`:

```shell
make
sudo make install
```

use std::env::args;

use gio::prelude::*;
use glib::clone;
use glib::translate::ToGlib;
use gtk::prelude::*;

fn build_ui(application: &gtk::Application, opts: &[String]) {
    let window = gtk::ApplicationWindowBuilder::new()
        .application(application)
        .title("SSH Askpass")
        .window_position(gtk::WindowPosition::Center)
        .can_focus(false)
        .has_focus(true)
        .resizable(false)
        .modal(true)
        .type_hint(gdk::WindowTypeHint::Dialog)
        .name("window")
        .build();

    let box_left_right = gtk::BoxBuilder::new()
        .orientation(gtk::Orientation::Horizontal)
        .spacing(2)
        .visible(true)
        .can_focus(false)
        .margin(6)
        .name("left_right")
        .build();

    window.add(&box_left_right);

    let box_controls = gtk::BoxBuilder::new()
        .orientation(gtk::Orientation::Vertical)
        .spacing(0)
        .visible(true)
        .can_focus(false)
        .homogeneous(true)
        .name("controls")
        .build();

    box_left_right.add(&box_controls);
    box_left_right.set_child_packing(&box_controls, true, true, 4, gtk::PackType::End);

    let label = gtk::LabelBuilder::new()
        .label("Please provide the passphrase for your SSH key.")
        .visible(true)
        .can_focus(false)
        .name("labelPassphrase")
        .build();

    let input = gtk::EntryBuilder::new()
        .visible(true)
        .can_focus(true)
        .valign(gtk::Align::Center)
        .visibility(false)
        .invisible_char(0x2022)
        .name("passphrase")
        .build();

    box_controls.add(&label);
    box_controls.add(&input);
    box_controls.set_child_packing(&label, false, true, 0, gtk::PackType::Start);
    box_controls.set_child_packing(&input, false, true, 0, gtk::PackType::Start);

    let box_buttons = gtk::BoxBuilder::new()
        .visible(true)
        .can_focus(false)
        .margin_top(6)
        .baseline_position(gtk::BaselinePosition::Bottom)
        .name("buttons")
        .build();

    box_controls.add(&box_buttons);
    box_controls.set_child_packing(&box_buttons, false, true, 0, gtk::PackType::End);

    let cancel_button = gtk::ButtonBuilder::new()
        .label("Cancel")
        .visible(true)
        .can_focus(true)
        .receives_default(true)
        .name("btnCancel")
        .build();

    let ok_button = gtk::ButtonBuilder::new()
        .label("OK")
        .visible(true)
        .can_focus(true)
        .receives_default(true)
        .name("btnOk")
        .build();

    box_buttons.add(&cancel_button);
    box_buttons.add(&ok_button);

    box_buttons.set_child_packing(&cancel_button, false, true, 0, gtk::PackType::Start);
    box_buttons.set_child_packing(&ok_button, false, true, 0, gtk::PackType::End);

    if opts.len() == 2 {
        label.set_text(opts.get(1).unwrap());
    }

    input.grab_focus();

    input.connect_activate(clone!(@weak application => move |input| {
        println!("{}", input.get_text().as_str());
        application.quit();
    }));

    cancel_button.connect_clicked(|_| {
        std::process::exit(1);
    });

    ok_button.connect_clicked(clone!(@weak application, @weak input => move |_| {
        println!("{}", input.get_text().as_str());
        application.quit();
    }));

    window.connect_key_release_event(|_, key| {
        if key.get_keyval().to_glib() == (gdk_sys::GDK_KEY_Escape as u32) {
            std::process::exit(1);
        }

        gtk::Inhibit(false)
    });

    window.show_all();
}

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");

        return;
    }

    let application =
        gtk::Application::new(Some("com.jgxvx.askpass"), Default::default()).expect("Init failed");

    application.connect_activate(|app| {
        if let Some(window) = app.get_window_by_id(0) {
            window.present();
        }
    });

    application.connect_startup(move |app| {
        build_ui(app, &args().collect::<Vec<_>>());
    });

    application.run(&[]);
}
